import { takeLatest} from 'redux-saga/effects';
import * as sagas from './sagas';
import primaryValues from '../constants/values';

export function* rootSaga () {
    yield takeLatest(primaryValues.$GET_LOGIN, sagas.fetchLogin);
};