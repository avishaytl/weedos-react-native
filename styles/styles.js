import { Dimensions } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';// ** device width&height **
import { Platform } from 'react-native';

const statusbar = getStatusBarHeight();
const width = Dimensions.get('window').width;
const height = Platform.OS !== 'ios' ? Dimensions.get('window').height + statusbar : Dimensions.get('window').height;
console.debug('is iphone - ' + Platform.OS);
console.log('status bar - ' + statusbar);
console.log('is Height - ' + height);
console.debug('is width - ' + width);

const primaryTheme = {
    $deviceWidth: width,
    $deviceHeight: height,
    $deviceStatusBar: statusbar, 
    $loginBackground: require('./images/background.png'),  
    $logo: require('./images/logo.png'), 
    $loginBtn: require('./images/login_btn.webp'), 
    $butz1: require('./images/butz1.webp'), 
    $butz2: require('./images/butz2.webp'), 
    $butz3: require('./images/butz3.webp'), 
    $butz4: require('./images/butz4.webp'), 
    $butz5: require('./images/butz5.webp'), 
    $butz6: require('./images/butz6.webp'), 
    $weedLeaf: require('./images/weed_leaf.webp'), 
    $pot: require('./images/pot.webp'), 
    $weed: require('./images/weed.png'), 
    $outdoor: require('./images/outdoor.jpg'), 
    $stepbystep: require('./images/stepbystep.jpg'), 
    $ledlight: require('./images/ledlight.jpg'),
    $cocoplanet: require('./images/cocoplanet.jpg'),
    $fertilizer: require('./images/fertilizer.jpg'),
    $waterpump: require('./images/waterpump.png'),
    $waterph: require('./images/waterph.png'),
    $tyingweed: require('./images/tyingweed.jpg'),
    $motherweed: require('./images/motherweed.jpg'),
    $diseases: require('./images/diseases.jpg'),
    $energy: require('./images/energy.jpg'),
    $growtent: require('./images/growtent.png'),
    $seeds: require('./images/seeds.png'),
    $growing: require('./images/growing.png'),
    $beforeflworing: require('./images/beforeflworing.png'),
    $flwoering: require('./images/flwoering.png'), 
    $login: require('./images/login.png'),
    $closebook: require('./images/closeBookIcon.png'), 
    $openbook: require('./images/openBookIcon.png'),
};

export default primaryTheme;
