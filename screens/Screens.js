import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { fromRight } from 'react-navigation-transitions';
import SigninScreen from '../pages/Login'; 
// import SignupScreen from '../pages/Signup';  
import MainScreen from '../pages/Main';
// import MainBuyTabStack from '../pages/MainBuy';
import CartScreen from '../pages/MainTab'; 
import GrowBookScreen from '../pages/GrowBook'; 
import SettingsScreen from '../pages/Settings'; 
import React from 'react';
import { View, Text, Image} from 'react-native'; 
import * as Icons from '@expo/vector-icons';
import primaryTheme from '../styles/styles';

const MainStack = createStackNavigator({
  MainScreen: {
    screen: MainScreen,
    navigationOptions: {
        header: null,
      },
  },
  MainTab: {
    screen: CartScreen,
    navigationOptions: {
        header: null,
      },
  }, 
},    
{ 
  transitionConfig: () => fromRight(),
  initialRouteName: 'MainScreen', 
});

// const NewsTabStack = createStackNavigator({
//   NewsScreen: {
//     screen: NewsScreen,
//     navigationOptions: {
//         header: null,
//       },
//   },
// },    
// { 
//   transitionConfig: () => fromRight(),
// });

const MainTabsStack = createBottomTabNavigator({
  Settings: {
    screen: SettingsScreen,
    navigationOptions: ()=>({
      tabBarLabel: ' ',
      tabBarIcon:({tintColor})=> 
        <View style={{ position:'absolute',  width:70, flexDirection: 'column', alignItems:'center', justifyContent:'center',}}>
            <Icons.Octicons style={{ color: tintColor,fontSize:24}} name={'settings'}/>
            <Text style={{color: tintColor,fontSize:16, margin:0}}>{'הגדרות'}</Text>
        </View> ,
      tabBarOptions:{   
        tabStyle:{ 
          flexDirection: 'row',
          alignItems:'center',
          justifyContent:'center',  
          borderWidth:0,
        },
        activeTintColor: '#fff', // active icon color
        inactiveTintColor: 'gray',  // inactive icon color
        borderWidth:0,
        style: {
            backgroundColor: '#000', // TabBar background, 
        },
        labelStyle:{
          fontSize: 16,   
        },
      }
    }),
  },
  Main: {
    screen: MainStack,
    navigationOptions: ()=>({ 
      tabBarLabel: ' ',
      tabBarIcon:({tintColor })=> 
        <View style={{ position:'absolute',  width:70, flexDirection: 'column', alignItems:'center', justifyContent:'center' ,paddingTop:5}}>
            {/* <Icons.Entypo style={{ color: tintColor,fontSize:24}} name={'leaf'}/>
            <Text style={{color: tintColor,fontSize:16, margin:0}}>{'WEEDOS'}</Text> */}
            <Image source={primaryTheme.$logo} resizeMode={'stretch'} style={{width:70,height:50}}/>
        </View> ,
      tabBarOptions:{   
        tabStyle:{ 
          flexDirection: 'row',
          alignItems:'center',
          justifyContent:'center',  
          borderWidth:0, 
        },
        activeTintColor: '#fff', // active icon color
        inactiveTintColor: 'gray',  // inactive icon color
        borderWidth:0,
        style: {
            backgroundColor: '#000', // TabBar background,
            borderWidth:0,
        },
        labelStyle:{
          fontSize: 16,   
        },
      }
    }),
  },
  GrowBook: {
    screen: GrowBookScreen,
    navigationOptions: ()=>({
      tabBarLabel: ' ',
      tabBarIcon:({tintColor})=> 
        <View style={{ position:'absolute',  width:70, flexDirection: 'column', alignItems:'center', justifyContent:'center',}}>
           {/* {tintColor === 'gray' ? <Image source={primaryTheme.$closebook} resizeMode={'stretch'} style={{width:40,height:40}}/> : <Image source={primaryTheme.$openbook} resizeMode={'stretch'} style={{width:70,height:40}}/> } */}
            <Icons.MaterialCommunityIcons style={{ color: tintColor,fontSize:24}} name={'book-open-page-variant'}/>
            <Text style={{color: tintColor,fontSize:16, margin:0}}>{'יומן גידול'}</Text>
        </View> ,
      tabBarOptions:{   
        tabStyle:{ 
          flexDirection: 'row',
          alignItems:'center',
          justifyContent:'center',    
        },
        activeTintColor: '#fff', // active icon color
        inactiveTintColor: 'gray',  // inactive icon color 
        style: {
            backgroundColor: '#000', // TabBar background, 
            borderWidth:0, 
        },
        labelStyle:{
          fontSize: 16,   
        },
      }
    }),
  },
},    
{
  initialRouteName: 'Main', 
});
 
// const MainBuyTabsStack = createBottomTabNavigator({
//   Settings: {
//     screen: SettingsScreen,
//     navigationOptions: ()=>({
//       tabBarLabel: ' ',
//       tabBarIcon:({tintColor})=> 
//         <View style={{ position:'absolute',  width:70, flexDirection: 'column', alignItems:'center', justifyContent:'center',}}>
//             <Icons.MaterialCommunityIcons style={{ color: tintColor,fontSize:24}} name={'clipboard-account'}/>
//             <Text style={{color: tintColor,fontSize:16, margin:0}}>{'חשבון'}</Text>
//         </View> ,
//       tabBarOptions:{   
//         tabStyle:{ 
//           flexDirection: 'row',
//           alignItems:'center',
//           justifyContent:'center',  
//           borderWidth:0,
//         },
//         activeTintColor: '#fff', // active icon color
//         inactiveTintColor: 'gray',  // inactive icon color
//         borderWidth:0,
//         style: {
//             backgroundColor: '#000', // TabBar background, 
//         },
//         labelStyle:{
//           fontSize: 16,   
//         },
//       }
//     }),
//   },
//   Cart: {
//     screen: CartScreen,
//     navigationOptions: ()=>({
//       tabBarLabel: ' ',
//       tabBarIcon:({tintColor})=> 
//         <View style={{ position:'absolute',  width:70, flexDirection: 'column', alignItems:'center', justifyContent:'center',}}>
//             <Icons.MaterialIcons style={{ color: tintColor,fontSize:24}} name={'shopping-cart'}/>
//             <Text style={{color: tintColor,fontSize:16 ,margin:0}}>{'עגלה'}</Text>
//         </View> ,
//       tabBarOptions:{   
//         tabStyle:{ 
//           flexDirection: 'row',
//           alignItems:'center',
//           justifyContent:'center',   
//         },
//         activeTintColor: '#fff', // active icon color
//         inactiveTintColor: 'gray',  // inactive icon color
//         borderWidth:0,
//         style: {
//             backgroundColor: '#000', // TabBar background, 
//             borderWidth:0,
//         },
//         labelStyle:{
//           fontSize: 16,   
//         },
//       }
//     }),
//   },
//   Main: {
//     screen: MainBuyTabStack,
//     navigationOptions: ()=>({ 
//       tabBarLabel: ' ',
//       tabBarIcon:({tintColor })=> 
//         <View style={{ position:'absolute',  width:70, flexDirection: 'column', alignItems:'center', justifyContent:'center',}}>
//             <Icons.Entypo style={{ color: tintColor,fontSize:24}} name={'shop'}/>
//             <Text style={{color: tintColor,fontSize:16, margin:0}}>{'חנויות'}</Text>
//         </View> ,
//       tabBarOptions:{   
//         tabStyle:{ 
//           flexDirection: 'row',
//           alignItems:'center',
//           justifyContent:'center',  
//           borderWidth:0, 
//         },
//         activeTintColor: '#fff', // active icon color
//         inactiveTintColor: 'gray',  // inactive icon color
//         borderWidth:0,
//         style: {
//             backgroundColor: '#000', // TabBar background,
//             borderWidth:0,
//         },
//         labelStyle:{
//           fontSize: 16,   
//         },
//       }
//     }),
//   },
// },    
// {
//   initialRouteName: 'Main', 
// });
 
const CenterAppStack = createStackNavigator({
  SigninScreen: { 
        screen: SigninScreen,
        navigationOptions: {
            header: null,
          },
    }, 
  MainScreen: {    
        screen: MainTabsStack,
        navigationOptions: {
            header: null,
          },
    },  
},    
{
  initialRouteName: 'SigninScreen',
  transitionConfig: () => fromRight(),
});
  const Screens = createAppContainer(CenterAppStack);
  export default Screens;