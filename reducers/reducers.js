import primaryValues from '../constants/values';
const initialState = {
  email: '',
  password: '',
  api_token: '',
};

const reducer = (state = initialState, action) => {
  switch(action.type){
    case primaryValues.$STARTING:
        return {
          ...state,
          loadingLogin: true,
        };
    case primaryValues.$LOGIN_RESULT:
        return {
          ...state,
          email: action.response.email,
          password: action.response.password,
          loginErrorMsg: '*',
          loadingLogin: false,
          api_token: action.response.token,
          loadingMain: true,
        };
    case primaryValues.$LOGIN_ERROR:
        return {
          ...state,
          email: '',
          password: '',
          loginErrorMsg: action.response.errorDisplay,
          loadingLogin: false,
        };
    default:
      return state;
  }
};

export default reducer;