import React from 'react';
import { View, StyleSheet, Text, Animated, Modal, TextInput,ImageBackground} from 'react-native';  
import * as Icons from '@expo/vector-icons';
import primaryTheme from '../styles/styles'; 
import ButtonApp from '../components/ButtonApp';
import { TouchableOpacity } from 'react-native-gesture-handler';
export default class storyView extends React.Component{
  constructor(props){
    super(props);
    this.state = {   
      info: this.props.info,
      activeStory:[true,false,false,false],
      storyActive: true,
    }  
    this.changeStory = this.changeStory.bind(this);
  }
  UNSAFE_componentWillMount(){  
  }   
  // componentWillUnmount(){
  //   for(let i = 0;i < this.state.info.options.length;i++)
  //     this.state.info.options[i].animeWidth.setValue(0); 
  // }
  getstorys = (options) =>{
    let counterstorys = []; 
    for(let i = 0;i < options.length;i++){
      counterstorys.push(
          <View key={options[i].key} style={{flex:1,backgroundColor:'rgba(0,0,0,0.2)',margin:1}}>
            <Animated.View style={{height:'100%',width: options[i].animeWidth,backgroundColor:'black'}}> 
            </Animated.View>
          </View>);
    }
    if(this.state.storyActive === true){ 
      Animated.timing(
        options[0].animeWidth,
        {toValue: primaryTheme.$deviceWidth/(parseInt(options.length)) - ((parseInt(options.length)) === 1 ? 22 : (parseInt(options.length)) === 2 ? 12 : (parseInt(options.length)) === 3 ? 10 : 8), duration: 5000}
      ).start((o1)=>{
        if((options.length === 2 || options.length === 3 || options.length === 4)){ 
          if(o1.finished && this.state.activeStory[0] === true){
            console.log('1finished' + o1.finished); 
            this.state.activeStory[0] = false;
            this.state.activeStory[1] = true;
            this.changeStory(2);
            Animated.timing(
              options[1].animeWidth,
              {toValue: primaryTheme.$deviceWidth/(parseInt(options.length)) - ((parseInt(options.length)) === 1 ? 22 : (parseInt(options.length)) === 2 ? 12 : (parseInt(options.length)) === 3 ? 10 : 8), duration: 5000}
            ).start((o2)=>{
              if(options.length === 3 || options.length === 4) {
                if(o2.finished && this.state.activeStory[1] === true){
                  console.log('2finished' + o2.finished);
                  this.state.activeStory[1] = false;
                  this.state.activeStory[2] = true;
                  this.changeStory(3);
                  Animated.timing(
                    options[2].animeWidth,
                    {toValue: primaryTheme.$deviceWidth/(parseInt(options.length)) - ((parseInt(options.length)) === 1 ? 22 : (parseInt(options.length)) === 2 ? 12 : (parseInt(options.length)) === 3 ? 10 : 8), duration: 5000}
                  ).start((o3)=>{
                    if(options.length === 4) {
                      if(o3.finished && this.state.activeStory[2] === true){
                        console.log('3finished' + o3.finished);
                        this.state.activeStory[2] = false;
                        this.state.activeStory[3] = true;
                        this.changeStory(4);
                        Animated.timing(
                          options[3].animeWidth,
                          {toValue: primaryTheme.$deviceWidth/(parseInt(options.length)) - ((parseInt(options.length)) === 1 ? 22 : (parseInt(options.length)) === 2 ? 12 : (parseInt(options.length)) === 3 ? 10 : 8), duration: 5000}
                        ).start();

                      }
                    }
                  });
                } 
              }
            });
          } 
        }
      });
    }
    else
      for(let i = 0;i < this.state.info.options.length;i++)
      Animated.timing(
        this.state.info.options[i].animeWidth
      ).stop(()=>this.getstorys(options));
                console.log(this.state.storyActive);   
    return counterstorys;
  }
  resetstorys = (options) =>{
    for(let i = 0;i < options.length;i++){ 
      options[i].animeWidth.setValue(0);
    } 
  }
  changeStory = (pos) =>{ 
    this.setState({activeStory: pos === 1 ? [true,false,false,false] : pos === 2 ? [false,true,false,false] : pos === 3 ? [false,false,true,false] : [false,false,false,true]}); 
  } 
    render(){   
          return(
          <View style={styles.container}>
            <View style={{position:'absolute',height:5,overflow:'visible',top:10,width: primaryTheme.$deviceWidth - 20,borderRadius:5,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
              {this.getstorys(this.state.info.options)}
            </View>
            <View style={{position:'absolute',top:15,left:10}}>
              <TouchableOpacity onPress={()=> {
                this.resetstorys(this.state.info.options);
                this.setState({activeStory:[true,false,false,false]});
                for(let i = 0;i < this.state.info.options.length;i++)
                  Animated.timing(
                    this.state.info.options[i].animeWidth
                  ).stop();
                this.props.storyView(false);
                }} >
                <Icons.FontAwesome name={'close'} style={{fontSize: 30,color:'rgba(0,0,0,0.5)'}}/>
              </TouchableOpacity> 
            </View>
              <Text>{this.state.info.title}</Text>
            <View>
              {this.state.activeStory[0] === true ? <Text>1</Text> : null} 
              {this.state.activeStory[1] === true ? <Text>2</Text> : null} 
              {this.state.activeStory[2] === true ? <Text>3</Text> : null} 
              {this.state.activeStory[3] === true ? <Text>4</Text> : null} 
            </View>
              <View style={{width:'100%',height:'90%',top:50,position:'absolute',backgroundColor:'rgba(0,0,0,0)'}}>
                <View style={{width:'100%',height:'100%',flexDirection:'row'}}>
                  <TouchableOpacity activeOpacity={1} style={{width: primaryTheme.$deviceWidth/3,height:'100%'}} onPress={()=>console.log('click1')}>

                  </TouchableOpacity>
                  <TouchableOpacity activeOpacity={1} style={{width: primaryTheme.$deviceWidth/3,height:'100%'}}> 
                  </TouchableOpacity>
                  <TouchableOpacity activeOpacity={1} style={{width: primaryTheme.$deviceWidth/3,height:'100%'}} onPress={()=>console.log('click3')}>

                  </TouchableOpacity>
                </View> 
              </View>
          </View> 
          ); 
    }
  }
  const styles = StyleSheet.create({
    container: {   
      zIndex:99999,
      position:'absolute',
      width: primaryTheme.$deviceWidth,
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar - 50,
      top:primaryTheme.$deviceStatusBar,
      backgroundColor:'white',
      justifyContent:'center',
      alignItems:'center'
    },  
  });
     