import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, FlatList, TextInput,ImageBackground} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import * as Icons from '@expo/vector-icons';
export default class LogoView extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
    }
  }
    render(){  
          return(
            <View style={{height:150,width:'100%'}}>
                        <FlatList 
                          style={{backgroundColor:'white'}} 
                          horizontal={true}
                          initialScrollIndex={this.props.items.length - 1}
                          data={this.props.items}
                          getItemLayout={(data, index) => ({ index, length: 150, offset: (150 * index) })}
                          renderItem={({item}) => (
                            <TouchableOpacity activeOpacity={0.8} key={item.key} onPress={()=>alert('פרטי מוצר - ' + item.title)}>
                              <View style={{backgroundColor: '#fff',width:150,height:150,justifyContent:'center',alignItems:'center'}}>
                                <Text>{item.title}</Text>
                                <View style={{width:'70%',height:100,backgroundColor:'black'}}>
                                    <View style={{height:55,backgroundColor:'#959595',justifyContent:'center',alignItems:'center'}}>
                                      <Icons.FontAwesome style={{color:'#dfdfdf',marginLeft:4}} name={'image'}/>
                                    </View>
                                    <View style={{height:45,backgroundColor:'#dfdfdf',justifyContent:'center',alignItems:'center'}}>
                                      <View style={styles.mainLogoTextView}> 
                                          <View style={{width:'90%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                                          <View style={{width:'100%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                                          <View style={{width:'80%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                                          <View style={{width:'90%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                                          <View style={{width:'80%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                                      </View> 
                                    </View>
                                </View>
                              </View>
                            </TouchableOpacity>
                          )}
                        />
                  </View>  
          ); 
    }
  }
  const styles = StyleSheet.create({ 
    mainLogoTextView:{
      alignItems:'flex-end', 
      width: '80%',
    }, 
  });
     