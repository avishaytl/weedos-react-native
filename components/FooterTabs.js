import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, View, TouchableOpacity, Text, ScrollView } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import primaryTheme from '../styles/styles';
import InputApp from './InputApp';
import ModalApp from './ModalApp';
import ErrorInputApp from './ErrorInputApp';
import ButtonApp from './ButtonApp';
import { LinearGradient } from 'expo-linear-gradient';
import primaryValues from '../constants/values';

let slides = [
  {
    key: 'country',
    title: 'Mobile',  
    error: 'Mobile error does not',
    pageSmallTitle: 'In Easy Steps',
  }, 
  {
    key: 'code',
    title: 'Code',  
    error: 'Code error does not',
    pageSmallTitle: 'In Easy Steps',
  },
  {
    key: 'email',
    title: 'Email',  
    error: 'Email error does not',
    pageSmallTitle: 'In Easy Steps',
  },
  {
    key: 'person',
    title: 'First Name', 
    title2: 'Last Name',  
    error: 'First/Last error does not', 
    pageSmallTitle: 'In Easy Steps', 
  },
  {
    key: 'password',
    title: 'Password',
    title2: 'Retype Password',  
    error: 'Password/Retype error does not',
    pageSmallTitle: 'In Easy Steps',
  }, 
  {
    key: 'terms',
    title: 'Terms of Use',   
    pageSmallTitle: 'Terms of Use',
  },
];

export default class FooterTabs extends React.Component {
  constructor(props){
    super(props);
    this.state={
        showSlides: true,
        inputPress: null,
        isModalVisible: false,
        data:{ 
            btnTopTitle: 'Confirmation',
            btnBottomTitle: 'Support',
            termsText: `In oculis quidem se repellere, idque instituit docere sic: omne animal, simul atque integre iudicante itaque earum rerum facilis est primum igitur, quid est laborum et harum quidem se esse vult, summumque malum et, quantum possit, a natura incorrupte atque insitam in gravissimo bello animadversionis.
            Et quidem exercitus quid ex eo delectu rerum, quem modo ista sis aequitate, quam interrogare aut fugit, sed quia consequuntur magni dolores suscipiantur maiorum dolorum effugiendorum gratia  quidem se oratio, tua praesertim, qui in animis nostris inesse notionem, ut labore et voluptatem et dolorem.
            Probabo, inquit, sic agam, ut ad id ne ferae quidem faciunt, ut ipsi auctori huius disciplinae placet: constituam, quid iudicat, quo pertineant non existimant oportere nimium nos causae confidere, sed quia non intellegamus, tu paulo ante cum a sapiente delectus, ut perspiciatis, unde omnis dolor.`,
            linkSendCode: 'Send me a mobile code',
            linkMarriageCode: 'Send the code for marriage',
            btnNext: 'Next',
            btnPrev: 'Back',
            county: '',
            mobile: '',
            code: '',
            email: '',
            full_name: '',
            password: '',
        }
    } 
    this.onChangeTextInput = this.onChangeTextInput.bind(this);
    this.renderSlide = this.renderSlide.bind(this);
    this._renderDoneButton = this._renderDoneButton.bind(this);
    this.changeModalView = this.changeModalView.bind(this);
  }
  changeModalView = (val) =>{ 
    this.setState({isModalVisible: val});
  }
  btnClick = (btnTitle) => { 
    console.log(btnTitle);
  }
  onChangeTextInput = (val, inputName) => { 
    switch(inputName){ 
        case 'Mobile':
            this.setState({data: {...this.state.data,mobile: val}});
        break;
        case 'Code':
            this.setState({data: {...this.state.data,code: val}});
        break;
        case 'Email':
            this.setState({data: {...this.state.data,email: val}});
        break;
        case 'First Name':
            this.setState({data: {...this.state.data,full_name: val}});
        break;
        case 'Password':
            this.setState({data: {...this.state.data,password: val}});
        break;
    }
  }
  renderSlide = ({ item }) => {
    if( item.key != 'person' && item.key != 'password' && item.key != 'terms')
        return (
            <View style={[styles.slide]}> 
                <Text style={[styles.smallTitle,{position:'absolute',left:27,top:40}]}>{item.pageSmallTitle}</Text>
                <ErrorInputApp value={item.error}/>
                <InputApp placeholder={item.title} theme={'active'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: item.title})}/> 
                <TouchableOpacity activeOpacity={0.5} onPress={()=> this.state.isModalVisible === false ? this.setState({isModalVisible: true}) : this.setState({isModalVisible: false})}>
                  <Text style={[styles.link,{opacity: item.key === 'country' || item.key === 'code' ? 1 : 0}]}>{item.key === 'country' || item.key === 'code' ? item.key === 'country' ? this.state.data.linkSendCode : this.state.data.linkMarriageCode : '_'}</Text> 
                </TouchableOpacity>
            </View>
        );
    else 
      if(item.key == 'person' || item.key == 'password')
          return (
              <View style={[styles.slide]}> 
              <Text style={[styles.smallTitle,{position:'absolute',left:27,top:40}]}>{item.pageSmallTitle}</Text>
                  <ErrorInputApp value={item.error}/>
                  <InputApp placeholder={item.title} theme={'active'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: item.title})}/>
                  <InputApp placeholder={item.title2} theme={'active'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: item.title2})}/> 
                  <Text style={[styles.link,{opacity: 0}]}>{'_'}</Text>
              </View>
          ); 
      else//terms tab
        return (
            <View style={[styles.slide,{height:'85%',justifyContent:'center',paddingTop:100}]}> 
                 <Text style={[styles.smallTitle,{position:'absolute',left:27,top:40}]}>{item.pageSmallTitle}</Text>
                 <ScrollView style={{width:'100%',paddingLeft:10,paddingRight:10 }}>
                      <Text style={styles.termsText}>{this.state.data.termsText}</Text>
                      <View style={{width: '100%',alignItems:'center'}}>
                        <ButtonApp name={this.state.data.btnTopTitle} theme={'active'} onPress={() => this.btnClick(this.state.data.btnTopTitle)}/>
                        <ButtonApp name={this.state.data.btnBottomTitle} theme={'passive'} onPress={() => this.btnClick(this.state.data.btnBottomTitle)}/>
                      </View>
                 </ScrollView>
            </View>
        ); 
  }
  onDone = () => { 
    this.setState({ showSlides: false });
    this.props.navigate('MainScreen');
  }
  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
          {this.state.isModalVisible === true ? <ModalApp data={this.state.data} changeModalView={this.changeModalView} isVisible={this.state.isModalVisible}/> : null} 
          <Text style={styles.text}>{this.state.data.btnNext}</Text>
      </View>
    );
  };
  _renderPrevButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Text style={styles.text}>{this.state.data.btnPrev}</Text>
      </View>
    );
  };
  _renderDoneButton = () => {  
    return (
      <View style={styles.buttonCircleDone}>
        <Ionicons
          name="md-checkmark"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  };

  render() { 
      return(
        <AppIntroSlider 
            renderItem={this.renderSlide} 
            slides={slides} 
            onDone={this.onDone}
            nextLabel={'המשך'}
            prevLabel={'חזור'} 
            doneLabel={'כניסה'}
            dotStyle={{backgroundColor: 'white'}}
            activeDotStyle={{backgroundColor: '#42c7de'}}
            showPrevButton  bottomButton />
      )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#bcbcbc',
    textTransform: 'uppercase',
  },
  buttonCircle: {   
    paddingTop:10,
    justifyContent: 'center',
    alignItems: 'center',
  }, 
  buttonCircleDone: {   
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  }, 
  slide:{
    height:370,
    width:'100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  link:{
    fontSize:20,
    textDecorationLine:'underline',
    fontWeight:'bold',
    color: '#42c7de',
  },
  termsText:{
    fontSize:17,
  },
  smallTitle:{
    fontSize: 20,
    fontWeight: 'bold'
  }
});
