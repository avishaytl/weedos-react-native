import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, FlatList, TextInput,ImageBackground} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import * as Icons from '@expo/vector-icons';
export default class LogoView extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
        items:[
            {key:'item1',title:'תבנית 1',img: primaryTheme.$themeDefault},
            {key:'item2',title:'תבנית 2',img: primaryTheme.$themeHila}, 
        ]
    }
    this.state.items = this.state.items.reverse();
  }
    render(){  
          return(
            <View style={{height:450,width:'100%'}}>
                        <FlatList 
                          style={{backgroundColor:'white'}} 
                          horizontal={true}
                          initialScrollIndex={this.state.items.length - 1}
                          data={this.state.items}
                          getItemLayout={(data, index) => ({ index, length: 200, offset: (450 * index) })}
                          renderItem={({item}) => (
                            <TouchableOpacity style={{height:450,width:400,justifyContent:'center',alignItems:'center'}} activeOpacity={0.8} key={item.key} onPress={()=>alert('פרטי מוצר - ' + item.title)}>
                              <ImageBackground source={item.img} resizeMode={'stretch'} style={{width:200,height:450,justifyContent:'flex-start',alignItems:'center'}}>
                                <Text style={{backgroundColor:'white',padding:2,borderBottomLeftRadius:5,borderBottomRightRadius:5}}>{item.title}</Text>
                                {/* <View style={{width:'70%',height:100,backgroundColor:'black'}}>
                                    <View style={{height:55,backgroundColor:'#959595',justifyContent:'center',alignItems:'center'}}>
                                      <Icons.FontAwesome style={{color:'#dfdfdf',marginLeft:4}} name={'image'}/>
                                    </View>
                                    <View style={{height:45,backgroundColor:'#dfdfdf',justifyContent:'center',alignItems:'center'}}>
                                      <View style={styles.mainLogoTextView}> 
                                          <View style={{width:'90%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                                          <View style={{width:'100%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                                          <View style={{width:'80%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                                          <View style={{width:'90%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                                          <View style={{width:'80%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                                      </View> 
                                    </View>
                                </View> */}
                              </ImageBackground>
                            </TouchableOpacity>
                          )}
                        />
                  </View>  
          ); 
    }
  }
  const styles = StyleSheet.create({ 
    mainLogoTextView:{
      alignItems:'flex-end', 
      width: '80%',
    }, 
  });
     