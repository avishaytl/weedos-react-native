import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput,ImageBackground} from 'react-native';  
export default class Counter extends React.Component{
  constructor(props){
    super(props);
    this.state = {
        timer: 0, 
    }
    this.changeCounter = this.changeCounter.bind(this);
  }
  UNSAFE_componentWillMount(){
      this.changeCounter();
  }
  componentWillUnmount(){
      clearInterval(this.counter);
  } 
toHHMMSS = (sec) => {
    var sec_num = parseInt(sec, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
}
  changeCounter = () =>{  
      this.counter = setInterval(()=>{ 
        const today = new Date();
        const tomorrow = new Date(today);
        tomorrow.setHours(8,0,0,0);
        tomorrow.setDate(tomorrow.getDate() + 1); 
        // console.log(today);
        // console.log(tomorrow);   
        // console.log(Math.abs(Math.abs(tomorrow-today)/1000).toFixed()); 
        // console.log(this.toHHMMSS(Math.abs(Math.abs(tomorrow-today)/1000).toFixed()));   
        this.setState({timer: this.toHHMMSS(Math.abs(Math.abs(tomorrow-today)/1000).toFixed())});
    },1000);
  }
    render(){  
          return(
            <View style={styles.container}> 
                <Text style={{fontWeight:'bold'}}>{this.state.timer}</Text>
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    container: {   
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center', 
    },  
  });
     