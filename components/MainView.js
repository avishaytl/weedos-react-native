import React from 'react';
import {  View, StyleSheet, Animated, Text, TouchableOpacity, Image, FlatList} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import { LinearGradient } from 'expo-linear-gradient';
export default class MainView extends React.Component{
  constructor(props){
    super(props);
    this.state = {  
        items:[
            {
              key:'1', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'צמח הקנאביס',
              info: 'מידע',
              readTime: '2',
              votes: '4.5',
              img: primaryTheme.$weed,
              options:[
                    {key: '1',label:'על הצמח'},
                    {key: '2',label:'זנים'}, 
                ] 
            },
            {
              key:'2', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'סוגי גידול', 
              info: 'מידע',
              readTime: '2.5',
              votes: '3.5',
              img: primaryTheme.$outdoor,
              options:[
                    {key: '1',label:'גידול פנים/חוץ'},
                    {key: '2',label:'חללי גידול'}, 
                ] 
            },
            {
              key:'3', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'שלב אחרי שלב',
              info: 'מידע',
              readTime: '1.5',
              votes: '5',
              img: primaryTheme.$stepbystep,
              options:[
                    {key: '1',label:'הנבטה'},
                    {key: '2',label:'גדילה'},
                    {key: '3',label:'טרום פריחה'},
                    {key: '4',label:'פריחה'},
                ]  
            },
            {
              key:'4', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'תאורת גידול', 
              info: 'מידע',
              readTime: '3.5',
              votes: '4.5',
              img: primaryTheme.$ledlight,
              options:[
                    {key: '1',label:'נורות גידול'}, 
                    {key: '2',label:'פלורסנט'}, 
                    {key: '3',label:'לד'}, 
                ]  
            },
            {
              key:'5', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'מצעי גידול', 
              info: 'מידע',
              readTime: '1.5',
              votes: '2.5',
              img: primaryTheme.$cocoplanet,
              options:[
                    {key: '1',label:'עציצים'}, 
                    {key: '2',label:'אדמה'}, 
                    {key: '3',label:'קוקו פרלייט'}, 
                    {key: '4',label:'הידרו'}, 
                ]  
            },
            {
              key:'6', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'דישון', 
              info: 'מידע',
              readTime: '2.5',
              votes: '5',
              img: primaryTheme.$fertilizer,
              options:[
                    {key: '1',label:'תזונה לצמח'}, 
                    {key: '2',label:'סוגי דשנים'},  
                ]  
            },
            {
              key:'7', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'השקיה', 
              info: 'מידע',
              readTime: '4.5',
              votes: '4.5',
              img: primaryTheme.$waterpump,
              options:[
                    {key: '1',label:'השקיה נכונה'}, 
                    {key: '2',label:'השקיה אוטומטית'}, 
                ]  
            },
            {
              key:'8', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'חומציות ומוליכות', 
              info: 'מידע',
              readTime: '3.5',
              votes: '3.5',
              img: primaryTheme.$waterph,
              options:[
                    {key: '1',label:'מדידת חומציות'}, 
                    {key: '2',label:'מוליכות חשמלית'},  
                ]  
            },
            {
              key:'9', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'גיזום וקשירות', 
              info: 'מידע',
              readTime: '2.5',
              votes: '5',
              img: primaryTheme.$tyingweed,
              options:[
                    {key: '1',label:'קיטום'}, 
                    {key: '2',label:'יישור ראש'}, 
                    {key: '3',label:'הסרת עלים'}, 
                ]  
            },
            {
              key:'10', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'צמחי אם', 
              info: 'מידע',
              readTime: '1.5',
              votes: '4.5',
              img: primaryTheme.$motherweed,
              options:[
                    {key: '1',label:'ייחורים'}, 
                ]  
            },
            {
              key:'11', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'מחלות', 
              info: 'מידע',
              readTime: '2',
              votes: '4.5',
              img: primaryTheme.$diseases,
              options:[
                  {key: '1',label:'דישון'}, 
                  {key: '2',label:'שורשים'}, 
                  {key: '3',label:'מזיקים'}, 
                  {key: '4',label:'הדברה'}, 
              ]  
            },
            {
              key:'12', 
              itemMarginAnime: new Animated.Value(0), 
              label: 'צריכת חשמל', 
              info: 'מידע',
              readTime: '1',
              votes: '3.5',
              img: primaryTheme.$energy,
              options:[ ]  
            },
        ]   
    } 
  }  
    render(){  
      return(
          <View style={{width:'100%',height:'100%'}}>  
            <FlatList    
              showsVerticalScrollIndicator={false}  
              data={this.state.items}
              getItemLayout={(data, index) => ({ index, length: 300, offset: (300 * index) })}
              renderItem={({item}) => (
                <TouchableOpacity onPress={()=>this.props.navigate('MainTab',{item: item})} activeOpacity={0.9} key={item.key} style={{width:'100%',height:300,justifyContent:'center',alignItems:'center' }}> 
                    <View style={{width:'98%',height:'98%',borderBottomColor:'rgba(0,0,0,0.1)',borderBottomWidth:1}}>
                        <View style={{width:'100%',height:'60%',backgroundColor:'#fff',justifyContent:'center',alignItems:'center',overflow:'hidden'}}> 
                            <Image style={{width:primaryTheme.$deviceWidth,top: primaryTheme.$deviceWidth > 500 ? 50 : 0}} resizeMode={'contain'} source={item.img}/>
                        </View>
                        <View style={{width:'100%',height:'40%',justifyContent:'flex-start',alignItems:'flex-end',padding:10,backgroundColor:'rgba(255,255,255,0.2)'}}>
                            <Text style={{fontWeight:'bold',fontSize:18,color:'#fff'}}>{item.label}</Text>
                            <Text style={{fontSize:12,color:'#fff'}}>{item.info}</Text>  
                            <Text style={{fontSize:12,position:'absolute',left:5,bottom:5,color:'#fff'}}>{`זמן קריאה: ` + item.readTime + ` דקות`}</Text>   
                            <Text style={{fontSize:12,position:'absolute',right:5,bottom:5,color:'#fff'}}>{`דירוג: ` + item.votes}</Text>         
                        </View>
                    </View>
                </TouchableOpacity>
            )}
            />  
          </View>
      );
    } 
}

  

  const styles = StyleSheet.create({
    container: {  
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent:'center',
      width: 150,
      height: 45,
      margin:5,
      borderRadius: 20,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 1.84,

      elevation: 3,
    },  
    btnText:{ 
        fontSize:20,
        fontWeight:'bold'
    }
  }); 