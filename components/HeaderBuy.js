import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput,ImageBackground} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import * as Icons from '@expo/vector-icons';
export default class HeaderBuy extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
    }
  }
    render(){  
          return(
            <View style={styles.container}>
                <View style={{flex:1,height:50,justifyContent:'flex-start',alignItems:'center',flexDirection:'row'}}> 
                    <TouchableOpacity onPress={()=>console.log('like1')}>
                        <View style={styles.btnCircle}>
                            <Icons.AntDesign style={[styles.btnIcon]} name={'like1'}/>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> this.props.navigate('search')}>
                        <View style={styles.btnCircle}>
                            <Icons.FontAwesome style={[styles.btnIcon]} name={'search'}/>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{flex:1,height:50,justifyContent:'flex-end',alignItems:'center',flexDirection:'row'}}> 
                    <TouchableOpacity style={{justifyContent:'flex-start',alignItems:'center',flexDirection:'row'}} onPress={()=>console.log('category')}> 
                        <Icons.MaterialCommunityIcons style={[styles.btnIcon]} name={'menu-down-outline'}/> 
                        <Text style={{color:'orange'}}>קטגוריות</Text>
                    </TouchableOpacity>
                    <ImageBackground  resizeMode={'stretch'} source={primaryTheme.$logo}  style={styles.logo}/>
                </View> 
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    container: {  
        position:'absolute',
        top: primaryTheme.$deviceStatusBar,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        width: primaryTheme.$deviceWidth,
        height: 50,   
        backgroundColor:'#252525', 
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 1.84,
  
        elevation: 3,
    }, 
    logo:{ 
        width:35,
        height:35,
        margin:5, 
    },
    btnCircle:{
        width:30,
        height:30,
        borderRadius:30/2,
        margin:5,
        // backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center'
    },
    btnIcon:{
        fontSize:20,
        color:'gray'
    }
  });
     