import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput,ImageBackground} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import * as Icons from '@expo/vector-icons';
export default class LogoView extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
    }
  }
    render(){  
          return(
            <View style={[styles.itemView,{flexDirection: this.props.imgPos === 'left' ? 'row' : 'row-reverse'}]}>
              <TouchableOpacity activeOpacity={0.8} onPress={()=> alert('בחר מוצר')} style={styles.itemViewImg}>
                <Text style={{color:'white',fontSize:12,opacity:0.5,marginBottom:2}}>{'מוצר'}</Text>
                <Icons.FontAwesome style={{color:'#dfdfdf',marginLeft:4}} name={'image'}/> 
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.8} onPress={()=> alert('בחר פרטי מוצר')} style={styles.itemViewText}>
                <View style={styles.logoTextViewTitle}>
                  <Text style={{color:'#7d7d7d',fontSize:12,opacity:0.5,marginBottom:2}}>{'פרטי מוצר'}</Text> 
                  <Icons.MaterialIcons style={{color:'#7d7d7d',marginLeft:4}} name={'text-fields'}/> 
                </View>
                <View style={styles.mainLogoTextView}>
                    <View style={{width:'90%',height:12,backgroundColor:'#5b5b5b',marginBottom:2}}></View>
                    <View style={{width:'80%',height:12,backgroundColor:'#5b5b5b',marginBottom:2}}></View>
                    <View style={{width:'90%',height:6,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                    <View style={{width:'80%',height:6,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                    <View style={{width:'90%',height:6,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                    <View style={{width:'80%',height:6,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                </View>
              </TouchableOpacity>
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    itemView:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
  
      },
      itemViewImg:{
        flex: 1, 
        height: 150,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center', 
        backgroundColor:'#7d7d7d'
      },
      itemViewText:{
        flex: 1, 
        height: 150,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent:'center', 
        backgroundColor:'#dfdfdf'
      },
      mainLogoTextView:{
        alignItems:'flex-end', 
        width: '80%',
      },
      logoTextViewTitle:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center', 
      }
  });
     