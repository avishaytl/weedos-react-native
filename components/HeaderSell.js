import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput,ImageBackground} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import * as Icons from '@expo/vector-icons';
export default class HeaderSell extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
    }
  }
    render(){  
          return(
            <View style={styles.container}>
                <View style={{flex:1,height:50,justifyContent:'flex-start',alignItems:'center',flexDirection:'row'}}> 
                    <TouchableOpacity style={{flex:1,height:50,justifyContent:'flex-start',alignItems:'center',flexDirection:'row'}} onPress={()=>alert('צבעים לבחירה')}>
                        <View style={styles.btnCircle}>
                            <Icons.Ionicons style={[styles.btnIcon]} name={'ios-color-palette'}/>
                        </View>
                        <Text style={{color:'white',opacity:0.5}}>צבעים</Text>
                    </TouchableOpacity> 
                </View>
                <View style={{flex:1,height:50,justifyContent:'flex-end',alignItems:'center',flexDirection:'row'}}>  
                    <TouchableOpacity style={{flex:1,height:50,justifyContent:'flex-end',alignItems:'center',flexDirection:'row'}} onPress={()=>this.props.changeModalView(true)}>
                        <Text style={{color:'white',opacity:0.5}}>תבנית</Text>
                        <View style={styles.btnCircle}>
                            <Icons.MaterialIcons style={[styles.btnIcon]} name={'photo-size-select-small'}/> 
                        </View>
                    </TouchableOpacity>
                </View> 
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    container: {  
        position:'absolute',
        top: primaryTheme.$deviceStatusBar,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        width: primaryTheme.$deviceWidth,
        height: 50,   
        backgroundColor:'#252525', 
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 1.84,
  
        elevation: 3,
    }, 
    logo:{ 
        width:35,
        height:35,
        margin:5, 
    },
    btnCircle:{
        width:30,
        height:30,
        borderRadius:30/2,
        margin:5,
        // backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center'
    },
    btnIcon:{
        fontSize:20,
        color:'white'
    }
  });
     