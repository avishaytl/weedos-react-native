import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput,ImageBackground} from 'react-native';  
import InputApp from '../components/InputApp'; 
import ButtonApp from '../components/ButtonApp'; 
import * as Icons from '@expo/vector-icons'; 
export default class FormBuy extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
    } 
  }
  UNSAFE_componentWillMount(){ 
  } 
    render(){  
          return(
            <View style={styles.container}>  
                <InputApp icon={<Icons.Foundation name={'address-book'} style={{fontSize:24,color:'black',marginLeft:5}}/>} placeholder={'שם פרטי'} theme={'passive'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: this.state.data.inputBottomTitle})}/>
                <InputApp icon={<Icons.Foundation name={'address-book'} style={{fontSize:24,color:'black',marginLeft:5}}/>} placeholder={'שם משפחה'} theme={'passive'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: this.state.data.inputBottomTitle})}/>
                <InputApp icon={<Icons.MaterialCommunityIcons name={'cake-variant'} style={{fontSize:24,color:'black',marginLeft:5}}/>} placeholder={'תאריך לידה'} theme={'passive'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: this.state.data.inputBottomTitle})}/>
                <InputApp icon={<Icons.Entypo name={'address'} style={{fontSize:24,color:'black',marginLeft:5}}/>} placeholder={'כתובת'} theme={'passive'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: this.state.data.inputBottomTitle})}/>
                <InputApp icon={<Icons.MaterialIcons name={'email'} style={{fontSize:24,color:'black',marginLeft:5}}/>} placeholder={'אימייל'} theme={'passive'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: this.state.data.inputBottomTitle})}/>
                <InputApp icon={<Icons.AntDesign name={'phone'} style={{fontSize:24,color:'black',marginLeft:5}}/>} placeholder={'טלפון'} theme={'passive'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: this.state.data.inputBottomTitle})}/>
                <InputApp icon={<Icons.Feather name={'lock'} style={{fontSize:24,color:'black',marginLeft:5}}/>} placeholder={'סיסמא'} theme={'passive'} onChangeText={this.onChangeTextInput} onPress={() => this.setState({inputPress: this.state.data.inputBottomTitle})}/>
                <ButtonApp icon={<Icons.MaterialCommunityIcons name={'menu-left'} style={{fontSize:24,color:'black',marginRight:5}}/>} name={'המשך'} theme={'passive'} onPress={() => this.props.navigate('MainScreen')}/>
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    container: {   
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent:'center', 
    },  
  });
     