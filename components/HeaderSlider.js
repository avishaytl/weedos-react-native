import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import TextTicker from 'react-native-text-ticker'

export default class HeaderSlider extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
        data:{
            rollText: 'GBP/USD: 1.2342123 NZD/USD 0.2395 GBP/USD: 1.2342 FACEBOOK/EUR: 1.2342123 TDU/SRT 0.2395 STOCKES/NTZ: 1.2342',
            btnLeftTitle: 'Trades',
            btnMidTitle: 'News',
            btnRightTitle: 'Settings',
        }
    }
  }
    render(){  
          return(
            <View style={styles.container}> 
               <TextTicker
                style={styles.text}
                duration={20000}
                loop
                bounce
                repeatSpacer={50}
                marqueeDelay={1000}
                > 
                {this.state.data.rollText}
        </TextTicker> 
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    container: {  
        position:'absolute',
        top: primaryTheme.$deviceStatusBar + 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        width: primaryTheme.$deviceWidth,
        height: 50, 
        backgroundColor:'#aef9ff',
    },
    text:{
        fontSize:16,
        fontWeight:'bold',
    }
  });
     