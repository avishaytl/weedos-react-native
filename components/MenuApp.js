import React from 'react';
import {   View, StyleSheet, Animated, Text, TouchableOpacity, Image, FlatList} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import { LinearGradient } from 'expo-linear-gradient';
import * as Icons from '@expo/vector-icons';  
export default class MenuApp extends React.Component{
  constructor(props){
    super(props);
    this.state = {
        menuAnime: this.props.menuAnime,
        items:[
            {
              key:'1', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.Entypo style={{ color: 'white',fontSize:18,marginLeft:4}} name={'leaf'}/>,
              label: 'צמח הקנאביס',
              options:[
                    {key: '1',label:'על הצמח'},
                    {key: '2',label:'זנים'}, 
                ] 
            },
            {
              key:'2', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.FontAwesome5 name={'cloud-sun'} style={{ color: '#fff',fontSize:17,marginLeft:4}}/>,
              label: 'סוגי גידול', 
              options:[
                    {key: '1',label:'גידול פנים/חוץ'},
                    {key: '2',label:'חללי גידול'}, 
                ] 
            },
            {
              key:'3', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.Entypo name={'book'} style={{ color: '#fff',fontSize:18,marginLeft:4}}/>,
              label: 'שלב אחרי שלב',
              options:[
                    {key: '1',label:'הנבטה'},
                    {key: '2',label:'גדילה'},
                    {key: '3',label:'טרום פריחה'},
                    {key: '4',label:'פריחה'},
                ]  
            },
            {
              key:'4', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.FontAwesome5 name={'lightbulb'} style={{ color: '#fff',fontSize:18,marginLeft:4}}/>,
              label: 'תאורת גידול', 
              options:[
                    {key: '1',label:'נורות גידול'}, 
                    {key: '2',label:'פלורסנט'}, 
                    {key: '3',label:'לד'}, 
                ]  
            },
            {
              key:'5', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.MaterialCommunityIcons name={'pot'} style={{ color: '#fff',fontSize:18,marginLeft:4}}/>,
              label: 'מצעי גידול', 
              options:[
                    {key: '1',label:'עציצים'}, 
                    {key: '2',label:'אדמה'}, 
                    {key: '3',label:'קוקו פרלייט'}, 
                    {key: '4',label:'הידרו'}, 
                ]  
            },
            {
              key:'6', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.FontAwesome5 name={'notes-medical'} style={{ color: '#fff',fontSize:18,marginLeft:5}}/>,
              label: 'דישון', 
              options:[
                    {key: '1',label:'תזונה לצמח'}, 
                    {key: '2',label:'סוגי דשנים'},  
                ]  
            },
            {
              key:'7', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.Ionicons name={'ios-water'} style={{ color: '#fff',fontSize:18,marginLeft:4}}/>,
              label: 'השקיה', 
              options:[
                    {key: '1',label:'השקיה נכונה'}, 
                    {key: '2',label:'השקיה אוטומטית'}, 
                ]  
            },
            {
              key:'8', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.MaterialIcons name={'opacity'} style={{ color: '#fff',fontSize:18,marginLeft:4}}/>,
              label: 'חומציות ומוליכות', 
              options:[
                    {key: '1',label:'מדידת חומציות'}, 
                    {key: '2',label:'מוליכות חשמלית'},  
                ]  
            },
            {
              key:'9', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.MaterialCommunityIcons name={'scissors-cutting'} style={{ color: '#fff',fontSize:18,marginLeft:4}}/>,
              label: 'גיזום וקשירות', 
              options:[
                    {key: '1',label:'קיטום'}, 
                    {key: '2',label:'יישור ראש'}, 
                    {key: '3',label:'הסרת עלים'}, 
                ]  
            },
            {
              key:'10', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.MaterialCommunityIcons name={'mother-nurse'} style={{ color: '#fff',fontSize:18,marginLeft:2}}/>,
              label: 'צמחי אם', 
              options:[
                    {key: '1',label:'ייחורים'}, 
                ]  
            },
            {
              key:'11', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.Ionicons name={'md-bug'} style={{ color: '#fff',fontSize:18,marginLeft:4}}/>,
              label: 'מחלות', 
              options:[
                  {key: '1',label:'דישון'}, 
                  {key: '2',label:'שורשים'}, 
                  {key: '3',label:'מזיקים'}, 
                  {key: '4',label:'הדברה'}, 
              ]  
            },
            {
              key:'12', 
              itemMarginAnime: new Animated.Value(0),
              icon: <Icons.SimpleLineIcons name={'energy'} style={{ color: '#fff',fontSize:18,marginLeft:2}}/>,
              label: 'צריכת חשמל', 
              options:[ ]  
            },
        ]   
    } 
  }  
  getChildOption = (options) =>{
      let objOptions = [];
    for(let i = 0;i < options.length;i++)
    objOptions.push(
        <TouchableOpacity onPress={()=> alert(options[i].label)} activeOpacity={1} key={options[i].key} style={{backgroundColor: '#000',width:'100%',height:40,justifyContent:'center',alignItems:'center',position:'absolute',top:40*(i + 1) ,zIndex:999999}}>
            <Text style={{color:'#c9c9c9'}}>{options[i].label}</Text> 
        </TouchableOpacity>);
    return objOptions;
  }
    render(){  
      return( 
        <Animated.View style={[styles.container,{left: primaryTheme.$deviceWidth}]}>  
          <View style={{height:'100%',width:200,top:35.65}}>
                   {/* <View style={{width:'100%',height:50,backgroundColor:'#252525',alignItems:'center',paddingTop:10,justifyContent:'center'}}> 
                      <Image style={{width:130,height:30,transform:[{rotate:'-5deg'}]}} resizeMode={'stretch'} source={primaryTheme.$logo}/> 
                  </View> */}
                  <FlatList   
                  data={this.state.items}
                  getItemLayout={(data, index) => ({ index, length: 40, offset: (40 * index) })}
                  renderItem={({item}) => (
                      <Animated.View key={item.key} style={{marginBottom: item.itemMarginAnime,width:200,height:40}}>
                          <TouchableOpacity activeOpacity={1} onPress={()=> { 
                              item.itemMarginAnime._value == 0 ?
                              Animated.timing(
                                  item.itemMarginAnime,
                                  {toValue: item.options.length * 40, duration: 400}
                                ).start() :
                                Animated.timing(
                                    item.itemMarginAnime,
                                    {toValue: 0, duration: 400}
                                  ).start(); 
                              }}>
                              <View style={{backgroundColor: '#000',height:40,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                                  <Text style={{color:'#fff',fontWeight:'bold',fontSize:16}}>{item.label}</Text> 
                                  {item.icon}
                              </View>
                          </TouchableOpacity> 
                            {this.getChildOption(item.options)}
                      </Animated.View>
                  )}
                  />      
                  </View>
        </Animated.View> 
      );
    } 
}

  

  const styles = StyleSheet.create({
    container: {    
      alignItems: 'center',
      justifyContent:'center', 
      // backgroundColor:'rgba(0,0,0,1)',
      // backgroundColor:'#252525', 
      width: 200, 
      height: primaryTheme.$deviceHeight  - primaryTheme.$deviceStatusBar - 50,
      // top: -primaryTheme.$deviceStatusBar,
      position:'absolute',
      alignItems:'flex-end',
      zIndex:99999,  
    },   
  }); 