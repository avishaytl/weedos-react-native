import React from 'react';
import {  View, StyleSheet, Animated, Text, TouchableOpacity, FlatList} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import { LinearGradient } from 'expo-linear-gradient';
export default class storyApp extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
        items:[
            {
                key:'1',  
                btnAnime: new Animated.Value(1), 
                title:'לגיליזציה בדרך',
                options:[
                    {
                        key:0,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:1,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:2,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:3,
                        animeWidth: new Animated.Value(0)
                    }
                ],
            }, 
            {
                key:'2',  
                btnAnime: new Animated.Value(1), 
                title:'גידול ביתי',
                options:[
                    {
                        key:0,
                        animeWidth: new Animated.Value(0)
                    }
                ],
            }, 
            {
                key:'3',  
                btnAnime: new Animated.Value(1), 
                title:'הקיץ בפתח',
                options:[
                    {
                        key:0,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:1,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:2,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:3,
                        animeWidth: new Animated.Value(0)
                    }
                ],
            }, 
            {
                key:'4', 
                btnAnime: new Animated.Value(1), 
                title:'השקיה נכונה' ,
                options:[
                    {
                        key:0,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:1,
                        animeWidth: new Animated.Value(0)
                    }
                ],
            }, 
            {
                key:'5',  
                btnAnime: new Animated.Value(1), 
                title:'הפיתרון המושלם',
                options:[
                    {
                        key:0,
                        animeWidth: new Animated.Value(0)
                    }
                ],
            }, 
            {
                key:'6', 
                btnAnime: new Animated.Value(1), 
                title:'חם בחוץ' ,
                options:[
                    {
                        key:0,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:1,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:2,
                        animeWidth: new Animated.Value(0)
                    }, 
                ],
            }, 
            {
                key:'7',  
                btnAnime: new Animated.Value(1), 
                title:'גידול חוץ',
                options:[
                    {
                        key:0,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:1,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:2,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:3,
                        animeWidth: new Animated.Value(0)
                    }
                ],
            }, 
            {
                key:'8',  
                btnAnime: new Animated.Value(1), 
                title:'ייחורים',
                options:[
                    {
                        key:0,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:1,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:2,
                        animeWidth: new Animated.Value(0)
                    },
                    {
                        key:3,
                        animeWidth: new Animated.Value(0)
                    }
                ],
            }, 
        ],
    } 
  }  
  btnClick = (item) =>{ 
    Animated.timing(
        item.btnAnime,
        {toValue: 0.95, duration: 100}
      ).start(()=>Animated.timing(
        item.btnAnime,
        {toValue: 1, duration: 100}
      ).start(()=>this.props.storyView(true,item)));
  }
    render(){  
      return(
          <View style={{width:'100%',height:'100%'}}> 
            <FlatList   
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            data={this.state.items}
            getItemLayout={(data, index) => ({ index, length: 150, offset: (150 * index) })}
            renderItem={({item}) => (
                <Animated.View key={item.key} style={{transform:[{scale: item.btnAnime}]}}> 
                    <TouchableOpacity onPress={()=>this.btnClick(item)} activeOpacity={0.9} style={{width:80,height:150,justifyContent:'flex-start',alignItems:'center'}}>
                        <View style={{width:80,height:100,justifyContent:'center',alignItems:'center',position:'absolute'}}>
                            <LinearGradient colors={['#5a733b','#a1c377','#456124','#5a733b','#a1c377']} style={{width:70,height:70,borderRadius:70/2,justifyContent:'center',alignItems:'center',position:'absolute'}}>
                                <LinearGradient colors={['#5a733b','#a1c377','#456124','#5a733b','#a1c377'].reverse()} style={{width:65,height:65,borderRadius:65/2,justifyContent:'center',alignItems:'center',position:'absolute'}}>
                                    <View style={{width:60,height:60,borderRadius:60/2,justifyContent:'center',alignItems:'center',backgroundColor:'#fff',position:'absolute'}}>
                                        <View style={{width:55,height:55,borderRadius:55/2,justifyContent:'flex-start',alignItems:'center',backgroundColor:'#252525',position:'absolute'}}>
                                            <Text style={{color:'white'}}>{item.key}</Text> 
                                        </View>
                                    </View>
                                </LinearGradient>
                            </LinearGradient>
                        </View>
                        <Text style={{color:'black',textAlign:'center',position:'absolute',top:85,fontSize:10,color:'white'}}>{item.title}</Text>
                    </TouchableOpacity>
                </Animated.View>
                
            )}
            />  
          </View>
      );
    } 
}

  

  const styles = StyleSheet.create({
    container: {  
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent:'center',
      width: 150,
      height: 45,
      margin:5,
      borderRadius: 20,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 1.84,

      elevation: 3,
    },  
    btnText:{ 
        fontSize:20,
        fontWeight:'bold'
    }
  }); 