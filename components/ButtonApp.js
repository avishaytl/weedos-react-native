import React from 'react';
import {  View, StyleSheet, Animated, Text, TouchableOpacity, Image} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import { LinearGradient } from 'expo-linear-gradient';
export default class ButtonApp extends React.Component{
  constructor(props){
    super(props);
    this.state = {
        btnAnime:  new Animated.Value(1),   
    } 
  } 
  btnAnime = () =>{
    Animated.timing(
      this.state.btnAnime,
      {toValue: 0.95, duration: 100}
    ).start(()=>Animated.timing(
      this.state.btnAnime,
      {toValue: 1, duration: 100}
    ).start());
  } 
  setBtnStyle = () =>{ 
    return this.props.theme === 'active' ? {
        btnTitleColor: 'white',
        btnBackcolors: ['#252525','#363636'],
        borderColor: 'rgba(0,0,0,0)',
    } : { 
        btnTitleColor: '#fff',
        btnBackcolors: ['#6d8346','#8fa861','#a0bd6b','#a0bd6b','#8fa861','#6d8346'],
        borderColor: '#3',
    };
  }
    render(){ 
      let style = this.setBtnStyle();
      return(
          <TouchableOpacity activeOpacity={0.95} onPressIn={this.btnAnime} onPress={this.props.onPress}> 
            <Animated.View style={{transform: [{ scale: this.state.btnAnime }]}}>
                <LinearGradient start={[0.1, 0.4]} end={[0.9, 0.1]} colors={['#fff','#c1c1c1','#fff']} style={[styles.container]}>
                  <LinearGradient  colors={style.btnBackcolors} style={[styles.container,{borderRadius: 20,width: 146,height:38}]}>
                      <Text style={[styles.btnText,{color: style.btnTitleColor}]}>{this.props.name}</Text>
                      {this.props.icon}
                  </LinearGradient> 
                </LinearGradient> 
            </Animated.View>
          </TouchableOpacity>
      );
    } 
}

  

  const styles = StyleSheet.create({
    container: {  
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent:'center',
      width: 150,
      height: 44,
      margin:5,
      borderRadius: 20,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 1.84,

      elevation: 3,
    },  
    btnText:{ 
        fontSize:20,
        fontWeight:'bold'
    }
  }); 