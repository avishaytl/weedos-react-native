import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Modal, TextInput,ImageBackground} from 'react-native'; 
import primaryTheme from '../styles/styles';  
import * as Icons from '@expo/vector-icons';
export default class LogoView extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
    }
  }
    render(){  
          return(
            <View style={styles.logoView}>
              <TouchableOpacity onPress={()=> alert('בחר תמונת לוגו')} activeOpacity={0.8} style={styles.logoImgView}>
                <Text style={{color:'white',fontSize:12,opacity:0.5,marginBottom:2}}>{'לוגו'}</Text>
                <Icons.FontAwesome style={{color:'#dfdfdf',marginLeft:4}} name={'image'}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> alert('בחר כותרת ראשית')} activeOpacity={0.8} style={styles.logoTextView}>
                <View style={styles.logoTextViewTitle}>
                  <Text style={{color:'#7d7d7d',fontSize:12,opacity:0.5,marginBottom:2}}>{'כותרת ראשית'}</Text> 
                  <Icons.MaterialIcons style={{color:'#7d7d7d',marginLeft:4}} name={'text-fields'}/> 
                </View>
                <View style={styles.mainLogoTextView}>
                    <View style={{width:'90%',height:6,backgroundColor:'#5b5b5b',marginBottom:2}}></View>
                    <View style={{width:'80%',height:6,backgroundColor:'#5b5b5b',marginBottom:2}}></View>
                    <View style={{width:'90%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                    <View style={{width:'80%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                    <View style={{width:'90%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                    <View style={{width:'80%',height:3,backgroundColor:'#aeaeae',marginBottom:2}}></View>
                </View>
              </TouchableOpacity>
            </View>
          ); 
    }
  }
  const styles = StyleSheet.create({
    logoView:{
        width: primaryTheme.$deviceWidth / 2,
        height: 80, 
        position:'absolute',
        backgroundColor: '#000',
        top: 110,
        zIndex:99999, 
        left: primaryTheme.$deviceWidth / 4,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 2,
        },
        shadowOpacity: 0.8,
        shadowRadius: 1.84,
        elevation: 3,
    },
    logoImgView:{
        width: 80,
        height: '100%',
        backgroundColor:'#7d7d7d',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    logoTextView:{
        flex:1,
        height: '100%',
        backgroundColor:'#dfdfdf' ,
        alignItems:'center',
        justifyContent:'center',
    },
    mainLogoTextView:{
      alignItems:'flex-end', 
      width: '80%',
    },
    logoTextViewTitle:{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center', 
    }
  });
     