import Screens from './screens/Screens';
import React from 'react';
import { Provider } from 'react-redux';
import configureStore from './store/store';
import { rootSaga } from './sagas/index';
import * as Font from 'expo-font';
export const store = configureStore();
store.runSaga(rootSaga);
export default class App extends React.Component{ 
    UNSAFE_componentWillMount() { 
    //   Font.loadAsync({
    //       'font-app': require('./assets/fonts/MADEWaffleSoftPERSONALUSE.otf'),
    //     }); 
    }
    render(){
        return(
            <Provider store={store}>
                <Screens/>
            </Provider>
        );
    }
}
