import React from 'react';
import { Animated, Keyboard, UIManager, Dimensions, View, StyleSheet, Image, TextInput, Text} from 'react-native';
import { connect } from 'react-redux'; 
import primaryTheme from '../styles/styles';
import { ScrollView } from 'react-native-gesture-handler';
const { State: TextInputState } = TextInput;
const getAvailableRoutes = navigation => {
  let availableRoutes = [];
  if (!navigation) return availableRoutes;

  const parent = navigation.dangerouslyGetParent();
  if (parent) {
    if (parent.router && parent.router.childRouters) {
      // Grab all the routes the parent defines and add it the list
      availableRoutes = [
        ...availableRoutes,
        ...Object.keys(parent.router.childRouters),
      ];
    }

    // Recursively work up the tree until there are none left
    availableRoutes = [...availableRoutes, ...getAvailableRoutes(parent)];
  }

  // De-dupe the list and then remove the current route from the list
  return [...new Set(availableRoutes)].filter(
    route => route !== navigation.state.routeName
  );
};

class MainTab extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
      shift: new Animated.Value(0), 
    } 
  } 
  UNSAFE_componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }
  getOptions = (options) =>{
    let labelOptions = [];
    for(let i = 0;i < options.length;i++) 
      labelOptions.push(<Text key={options[i].key} style={{fontSize:16,fontWeight:'bold'}}>{options[i].label}</Text>); 
    return labelOptions;
  }
    render(){
      const  item  = this.props.navigation.state.params.item;
      const { navigate } = this.props.navigation;
      return(
          <View style={styles.container}>  
            <View style={styles.main}>
              <ScrollView style={{width:'100%',height:'100%'}}>
                  <View style={styles.topView}>
                    <Image style={{width:primaryTheme.$deviceWidth }} resizeMode={'contain'} source={item.img}/>
                  </View>
                  <View style={styles.infoView}>
                    <Text style={{fontSize:22,fontWeight:'bold'}}>{item.label}</Text>
                    <Text>{item.info}</Text>
                    {this.getOptions(item.options)}
                  </View>
              </ScrollView>  
            </View>    
          </View>
      );
    }
  
    handleKeyboardDidShow = (event) => {
      const { height: windowHeight } = Dimensions.get('window');
      const keyboardHeight = event.endCoordinates.height;
      const currentlyFocusedField = TextInputState.currentlyFocusedField();
      UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
        const fieldHeight = height;
        const fieldTop = pageY;
        const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight);
        if (gap >= 0) {
          return;
        }
        Animated.timing(
          this.state.shift,
          {
            toValue: gap - 100,
            duration: 500,
            useNativeDriver: true,
          }
        ).start();
      });
    }
    
    handleKeyboardDidHide = () => {
      Animated.timing(
        this.state.shift,
        {
          toValue: 0,
          duration: 500,
          useNativeDriver: true,
        }
      ).start();
    } 
}

  

  const styles = StyleSheet.create({
    container: { 
      height: primaryTheme.$deviceHeight,
      backgroundColor:'#252525',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent:'flex-start',
    }, 
    main:{
      width:'100%',
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar - 50,
      top: primaryTheme.$deviceStatusBar,
      backgroundColor:'#fff', 
    },
    topView:{
      width:'100%', 
      height:250,
      overflow:'hidden',
      alignItems: 'center',
      justifyContent:'center',
    },
    infoView:{
      padding:10,
      alignItems: 'flex-end',
      justifyContent:'flex-start', 
    }
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  };
  // Exports
  export default connect(mapStateToProps)(MainTab);