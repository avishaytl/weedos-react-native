import React from 'react';
import { Animated, Keyboard, UIManager, Dimensions, View, StyleSheet, ImageBackground, TouchableOpacity, TextInput, Text, Image, FlatList,SafeAreaView} from 'react-native';
import { connect } from 'react-redux'; 
import primaryTheme from '../styles/styles';
import { ScrollView } from 'react-native-gesture-handler';
import { disableExpoCliLogging } from 'expo/build/logs/Logs'; 
import * as Icons from '@expo/vector-icons';  
const { State: TextInputState } = TextInput;
// const getAvailableRoutes = navigation => {
//   let availableRoutes = 'גדילה, שבוע 1';
//   if (!navigation) return availableRoutes;

//   const parent = navigation.dangerouslyGetParent();
//   if (parent) {
//     if (parent.router && parent.router.childRouters) {
//       // Grab all the routes the parent defines and add it the list
//       availableRoutes = [
//         ...availableRoutes,
//         ...Object.keys(parent.router.childRouters),
//       ];
//     }

//     // Recursively work up the tree until there are none left
//     availableRoutes = [...availableRoutes, ...getAvailableRoutes(parent)];
//   }

//   // De-dupe the list and then remove the current route from the list
//   return [...new Set(availableRoutes)].filter(
//     route => route !== navigation.state.routeName
//   );
// }; 
          /* {getAvailableRoutes(this.props.navigation).map(route => (
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate(route)}
              key={route}
              style={{
                backgroundColor: '#fff',
                padding: 10,
                margin: 10,
              }}
            >
              <Text>{route}</Text>
            </TouchableOpacity>
          ))} */

class GrowBook extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
      shift: new Animated.Value(0), 
      text:`החלטתם לגדל?  מעולה! כאן תוכלו לבצע צ’ק ליסט ראשוני לציוד שברשותכם, לעקוב אחרי הצמח שלב אחרי שלב, לחשב כמות דישון מומלצת, ליצור טבלאות מעקב, לרשום ההערות ועוד.`,
      items:[
        {
          key:'1',  
        },
        {
          key:'2',
          title:'setup',
          topInfo: `סט אפ - הכל מתחיל כאן, זה יכול להיות ארון ישן, מארז מחשב או אוהל גידול מקצועי.`,
          urlImage: primaryTheme.$growtent,
          imgWidth: 180,
          imgHight: 180,
          viewAnime: new Animated.Value(180),
          marginAnime: new Animated.Value(0),
          arrowAnime: new Animated.Value(0), 
        },
        {
          key:'3',
          title:'sprouting',
          topInfo: `הנבטה - ישן דרכים שונות להנביט זרעים, לרוב זה לוקח עד 3 ימים אך יכול לקחת גם 10 ימים.`,
          urlImage: primaryTheme.$seeds,
          imgWidth: 130,
          imgHight: 65,
          viewAnime: new Animated.Value(180),
          marginAnime: new Animated.Value(0),
          arrowAnime: new Animated.Value(0), 
        },
        {
          key:'4',
          title:'growing',
          topInfo: `הנבטה - ישן דרכים שונות להנביט זרעים, לרוב זה לוקח עד 3 ימים אך יכול לקחת גם 10 ימים.`,
          urlImage: primaryTheme.$growing ,
          imgWidth: 150,
          imgHight: 150, 
          viewAnime: new Animated.Value(180),
          marginAnime: new Animated.Value(0),
          arrowAnime: new Animated.Value(0), 
        },
        {
          key:'5',
          title:'beforeflowering',
          topInfo: `הנבטה - ישן דרכים שונות להנביט זרעים, לרוב זה לוקח עד 3 ימים אך יכול לקחת גם 10 ימים.`,
          urlImage: primaryTheme.$beforeflworing,
          imgWidth: 150,
          imgHight: 150,
          viewAnime: new Animated.Value(180),
          marginAnime: new Animated.Value(0),
          arrowAnime: new Animated.Value(0),  
        },
        {
          key:'6',
          title:'flowering',
          topInfo: `הנבטה - ישן דרכים שונות להנביט זרעים, לרוב זה לוקח עד 3 ימים אך יכול לקחת גם 10 ימים.`,
          urlImage: primaryTheme.$flwoering,
          imgWidth: 150,
          imgHight: 150,
          viewAnime: new Animated.Value(180),
          marginAnime: new Animated.Value(0),
          arrowAnime: new Animated.Value(0),  
        }, 
      ]
    } 
  } 
  UNSAFE_componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }
  readMoreAnime = (height,margin,arrow) =>{ 
    Animated.timing(
      height,
      {
        toValue: height._value === 180 ? 400 : 180,
        duration: 500, 
      }
    ).start();
    Animated.timing(
      margin,
      {
        toValue: margin._value === 0 ? 220 : 0,
        duration: 500, 
      }
    ).start(); 
    Animated.timing(
      arrow,
      {
        toValue: margin._value === 0 ? 3.1 : 0,
        duration: 500, 
        useNativeDriver : true,
      }
    ).start(); 
  }
  firstItemView = ()=> {
    let data = [
      {key:'1',date:'24/3/2020',title:'Bubba Kush',stage:'פריחה, שבוע 1'},
      {key:'2',date:'12/4/2020',title:'OG Kush',stage:'גדילה, שבוע 2'},
      {key:'3',date:'15/2/2020',title:'Cheese',stage:'פריחה, שבוע 1'},
      {key:'4',date:'3/1/2020',title:'White Widow',stage:'גדילה, שבוע 3'},
      {key:'5',date:'24/3/2020',title:'Bubba Kush',stage:'פריחה, שבוע 3'},
      {key:'6',date:'12/4/2020',title:'OG Kush',stage:'גדילה, שבוע 2'},
      {key:'7',date:'15/2/2020',title:'Cheese',stage:'גדילה, שבוע 1'},
      {key:'8',date:'3/1/2020',title:'White Widow',stage:'פריחה, שבוע 3'},
      {key:'9',date:'24/3/2020',title:'Bubba Kush',stage:'גדילה, שבוע 1'},
      {key:'10',date:'12/4/2020',title:'OG Kush',stage:'פריחה, שבוע 2'}, 
    ];
    return ( 
        <View style={{width:'100%',height:'100%',alignItems:'center',justifyContent:'flex-start',padding:10}}>
            <Text style={{color:'white',textAlign:'center',fontSize:24,fontWeight:'bold',maxWidth: primaryTheme.$deviceWidth - 100,height:40}}>יומן גידול</Text>
            <Text style={{color:'white',textAlign:'center',fontSize:14,fontWeight:'bold',maxWidth: primaryTheme.$deviceWidth - 100,height:100}}>{this.state.text}</Text>
            <View style={{width:'100%',height:330,backgroundColor:'rgba(255,255,255,0.2)', alignItems: 'center', justifyContent: 'center'}}>
              <View style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'column' }}>
                <View style={{ height: 30,  flexDirection: 'row' }}>
                  <Text style={[styles.tableText,{fontSize:18,fontWeight:'bold'}]}>{'תאריך שתילה'}</Text>
                  <Text style={[styles.tableText,{fontSize:18,fontWeight:'bold'}]}>{'זן הצמח'}</Text>
                  <Text style={[styles.tableText,{fontSize:18,fontWeight:'bold'}]}>{'שלב נוכחי'}</Text>
                </View>
                  {data.map((plant) => { // This will render a row for each data element.
                      return (
                        <TouchableOpacity key={plant.key}>
                          <View style={{ height: 30,  flexDirection: 'row' }}>
                            <Text style={styles.tableText}>{plant.date}</Text>
                            <Text style={styles.tableText}>{plant.title}</Text>
                            <Text style={[styles.tableText]}>{plant.stage}</Text>
                          </View> 
                        </TouchableOpacity>
                        );
                  })}
              </View>
            </View>
        </View>
    );
}
    render(){
      const { shift } = this.state;
      const { navigate } = this.props.navigation; 
      return(
          <View style={styles.container}>   
          <View style={styles.main}>
            <ImageBackground source={primaryTheme.$loginBackground} resizeMode={'stretch'} style={styles.mainBackground}> 
            <SafeAreaView  style={{width:'100%',height:'100%'}}>
              <FlatList    
                  style={{width:'100%',height:'100%'}} 
                  data={this.state.items}
                  getItemLayout={(data, index) => ({ index, length: index === 0 ? 500 : 200, offset: (index === 0 ? 500 : 200 * index) })}
                  renderItem={({item}) => (
                    <Animated.View key={item.key} style={{
                      width:'100%',
                      height: item.key !== '1' ? 200 : 500, 
                      marginBottom: item.marginAnime,
                      justifyContent:'flex-start', 
                      alignItems: parseInt(item.key) % 2 === 1 && item.key !== '1' ? 'flex-end' : parseInt(item.key) % 2 === 0 ? 'flex-start' : 'center' }}> 
                    {item.key !== '1' ? 
                    <Animated.View style={{
                      width: primaryTheme.$deviceWidth - 100,
                      height: item.viewAnime, 
                      alignItems:'flex-start',
                      justifyContent:'flex-start',
                      backgroundColor: 'rgba(255,255,255,0.2)'}}> 
                      {parseInt(item.key) % 2 === 1 ? 
                      <View style={{width:'100%',height:180, flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                        <TouchableOpacity onPress={()=>this.readMoreAnime(item.viewAnime,item.marginAnime,item.arrowAnime)} style={{padding:10,width:'50%',height:'100%',alignItems:'center',justifyContent:'center' }}>
                          <Text style={{color:'white',fontWeight:'bold',fontSize:18}}>{item.title}</Text> 
                          <Text style={{color:'white',textAlign: 'right'}}>{item.topInfo}</Text> 
                        </TouchableOpacity>  
                         <View style={{width:'50%',height:'100%',alignItems:'center',justifyContent:'center'}}>
                            <Image source={item.urlImage} resizeMode={'stretch'} style={{width: item.imgWidth,height: item.imgHight}}/>
                        </View>
                      </View> :  
                       <View style={{width:'100%',height:180,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                        <View style={{width:'50%',height:'100%',alignItems:'center',justifyContent:'center'}}>
                            <Image source={item.urlImage} resizeMode={'stretch'} style={{width: item.imgWidth,height: item.imgHight}}/>
                        </View>
                      <TouchableOpacity onPress={()=>this.readMoreAnime(item.viewAnime,item.marginAnime,item.arrowAnime)} style={{padding:10,width:'50%',height:'100%',alignItems:'center',justifyContent:'center' }}>
                          <Text style={{color:'white',fontWeight:'bold',fontSize:18}}>{item.title}</Text> 
                          <Text style={{color:'white',textAlign: 'right'}}>{item.topInfo}</Text>  
                      </TouchableOpacity>  
                    </View>}
                      <TouchableOpacity onPress={()=>this.readMoreAnime(item.viewAnime,item.marginAnime,item.arrowAnime)} style={parseInt(item.key) % 2 === 1 ? {width:30,height:30,left:0,position:'absolute',bottom:0}:{width:30,height:30,right:0,position:'absolute',bottom:0}}> 
                        <Animated.View style={{flex:1,transform:[{rotate: item.arrowAnime}],alignItems:'center',justifyContent:'center'}}>
                          <Icons.MaterialIcons name={'keyboard-arrow-up'} style={{color:'white',position:'absolute',fontSize:20, top:10}}/>
                          <Icons.MaterialIcons name={'keyboard-arrow-up'} style={{color:'white',position:'absolute',fontSize:20, }}/>
                        </Animated.View>  
                      </TouchableOpacity>
                    </Animated.View> :
                       this.firstItemView()}   
                  </Animated.View>
                  )} 
                />  
                </SafeAreaView > 
              </ImageBackground>
            </View>
          </View>
      );
    }
    handleKeyboardDidShow = (event) => {
      const { height: windowHeight } = Dimensions.get('window');
      const keyboardHeight = event.endCoordinates.height;
      const currentlyFocusedField = TextInputState.currentlyFocusedField();
      UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
        const fieldHeight = height;
        const fieldTop = pageY;
        const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight);
        if (gap >= 0) {
          return;
        }
        Animated.timing(
          this.state.shift,
          {
            toValue: gap - 100,
            duration: 500,
            useNativeDriver: true,
          }
        ).start();
      });
    }
    
    handleKeyboardDidHide = () => {
      Animated.timing(
        this.state.shift,
        {
          toValue: 0,
          duration: 500,
          useNativeDriver: true,
        }
      ).start();
    } 
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'#252525',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent:'center',
    }, 
    main:{
      width:'100%',
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar - 50,
      top:primaryTheme.$deviceStatusBar,
      position:'absolute',
      alignItems: 'center',
      justifyContent:'flex-start', 
      backgroundColor:'#000',
    },
    mainBackground:{
      width:'100%',
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar - 50, 
      backgroundColor:'#000',
      alignItems: 'center',
      justifyContent:'flex-start', 
    },   
    tableText:{
      fontSize:16,
      color:'white',
      flex:1,
      textAlign:'center'
    }
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  };
  // Exports
  export default connect(mapStateToProps)(GrowBook);