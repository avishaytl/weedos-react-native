import React from 'react';
import { Animated, Keyboard, UIManager, Dimensions, View, StyleSheet, TextInput, Text, ImageBackground, Image} from 'react-native';
import { connect } from 'react-redux'; 
import primaryTheme from '../styles/styles';
import ButtonApp from '../components/ButtonApp';
import InputApp from '../components/InputApp';  
import ModalApp from '../components/ModalApp'; 
import Counter from '../components/Counter';  
import { TouchableOpacity } from 'react-native-gesture-handler';
import FadeIn from 'react-native-fade-in-image';
import { LinearGradient } from 'expo-linear-gradient';
import Flag from 'react-native-flags';
import * as Icons from '@expo/vector-icons'; 
const { State: TextInputState } = TextInput;  
class Login extends React.Component{
  constructor(props){
    super(props);
    this.state = {  
      shift: new Animated.Value(0),    
      text: `המידע באפליקצית ווידו'ס נאסף על ידי סטלן שרצה לחקור וללמוד את העולם המופלא של צמח הקאנביס, האפליקציה אינה ממליצה או מעודדת לעבור על החוק והשימוש בה הוא למטרת למידה בלבד. באמצעות לחיצה על "כניסה" אנו רואים בזאת הסכמה והבנה לכך שלא ניקח אחריות על כל נזק או הפסד לכאורה שנגרם משימוש במידע שבאפליקציה.`,
    } 
  }  
  UNSAFE_componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);  
  }

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
    clearInterval(this.btnAnime);
  }
  btnClick = (btnTitle, navigate) => {
    btnTitle === this.state.data.btnBottomTitle ? this.signinAnime('start') : this.signupAnime('start');
  }
  changeModalView = (val) =>{ 
    this.setState({isModalVisible: val});
  }
  onChangeTextInput = (val, inputName) => {
    console.log(val,inputName);
    console.log(this.state.inputPress);
  } 
    render(){ 
      const { shift } = this.state;
      const { navigate } = this.props.navigation;
      return(
          <View style={styles.container}> 
            {this.state.isModalVisible === true ? <ModalApp data={this.state.data} changeModalView={this.changeModalView} isVisible={this.state.isModalVisible}/> : null} 
            <ImageBackground source={primaryTheme.$login} style={styles.mainBackground}>  
                {/* <Image style={{width:350,height:80,transform:[{rotate:'-5deg'}]}} resizeMode={'stretch'} source={primaryTheme.$logo}/>  */}
                {/* <Image style={{width:40,height:30,transform:[{rotate:'-5deg'}],position:'absolute',top:10}} resizeMode={'stretch'} source={primaryTheme.$butz1}/>  */}
                {/* <Image style={{width:40,height:40,transform:[{rotate:'-5deg'}],position:'absolute',top:10}} resizeMode={'stretch'} source={primaryTheme.$butz2}/> 
                <Image style={{width:40,height:20,transform:[{rotate:'-5deg'}],position:'absolute',top:10}} resizeMode={'stretch'} source={primaryTheme.$butz3}/> 
                <Image style={{width:20,height:25,transform:[{rotate:'-5deg'}],position:'absolute',top:10}} resizeMode={'stretch'} source={primaryTheme.$butz4}/> 
                <Image style={{width:30,height:30,transform:[{rotate:'-5deg'}],position:'absolute',top:10}} resizeMode={'stretch'} source={primaryTheme.$butz5}/> 
                <Image style={{width:50,height:60,transform:[{rotate:'-5deg'}],position:'absolute',top:10}} resizeMode={'stretch'} source={primaryTheme.$butz6}/>  */}
                <Text style={styles.text}>{this.state.text}</Text>  
                <View style={{paddingTop:30}}>
                  <ButtonApp onPress={()=> navigate('Main')} name={'כניסה'} /> 
                </View>
            </ImageBackground>
          </View>
      );
    } 

    handleKeyboardDidShow = (event) => {
        const { height: windowHeight } = Dimensions.get('window');
        const keyboardHeight = event.endCoordinates.height;
        const currentlyFocusedField = TextInputState.currentlyFocusedField();
        UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
          const fieldHeight = height;
          const fieldTop = pageY;
          let temp = this.state.inputPress === this.state.data.inputTopTitle ? 20 : -50;
          const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight + temp);
          if (gap >= 0) {
            return;
          }
          Animated.timing(
            this.state.logoAnime,
            {
              toValue: 50,
              duration: 400, 
            }
          ).start();
          Animated.timing(
            this.state.textOpacity,
            {
              toValue: 0,
              duration: 400, 
            }
          ).start();
          Animated.timing(
            this.state.shift,
            {
              toValue: gap - 80,
              duration: 500,
              useNativeDriver: true,
            }
          ).start();
        });
      }
      
      handleKeyboardDidHide = () => {
        Animated.timing(
          this.state.logoAnime,
          {
            toValue: 0,
            duration: 400, 
          }
        ).start();
        Animated.timing(
          this.state.textOpacity,
          {
            toValue: 1,
            duration: 900, 
          }
        ).start();
        Animated.timing(
          this.state.shift,
          {
            toValue: 0,
            duration: 500,
            useNativeDriver: true,
          }
        ).start();
      } 
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor:'#252525', 
    }, 
    mainBackground:{
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar,
      top: primaryTheme.$deviceStatusBar,
      width:'100%', 
      backgroundColor:'white',
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
    }, 
    text:{
      maxWidth:300,
      fontSize:16,
      textAlign:'center',
      paddingTop:140, 
      color:'white'
    } 
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  }; 
  export default connect(mapStateToProps)(Login);