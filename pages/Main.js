import React from 'react';
import { Animated, Keyboard, UIManager, Dimensions, View, StyleSheet, TextInput, Text, ScrollView, FlatList, TouchableOpacity, ImageBackground} from 'react-native';
import { connect } from 'react-redux'; 
import ModalApp from '../components/ModalApp';
import StoryView from '../components/StoryView';
import MainView from '../components/MainView';
import StoryApp from '../components/StoryApp';
import MenuApp from '../components/MenuApp';
import primaryTheme from '../styles/styles'; 
import * as Icons from '@expo/vector-icons';  
const { State: TextInputState } = TextInput;
class MainSell extends React.Component{
  constructor(props){
    super(props);
    this.state = { 
      storyView: false,
      storyInfo:{ 
      },
      shift: new Animated.Value(0), 
      isModalVisible: false,  
      menuAnime: new Animated.Value(primaryTheme.$deviceWidth),
      mainAnime: new Animated.Value(0),
      menuPosition: 'in',
      topTitle: 'מומחים משתפים'
    }   
  }  
  changeModalView = (val) =>{ 
    this.setState({isModalVisible: val});
  }
  UNSAFE_componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
  } 
  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }
  menuAnime = () =>{
    if(this.state.menuPosition === 'in'){ 
      Animated.timing(
        this.state.mainAnime,
        {toValue: -200, duration: primaryTheme.$deviceWidth * 2}
      ).start();
      this.setState({menuPosition: 'out'});
    }else{ 
      Animated.timing(
        this.state.mainAnime,
        {toValue: 0, duration: primaryTheme.$deviceWidth * 2}
      ).start();
      this.setState({menuPosition: 'in'});
    }
  }
  storyView = (val,item) =>{
    if(val === true)
      this.setState({storyInfo: item}); 
    this.setState({storyView: val});
  }
    render(){
      const { shift } = this.state;
      const { navigate } = this.props.navigation;
      return(
          <Animated.View style={[styles.container]}> 
          {this.state.storyView === true ? <StoryView info={this.state.storyInfo} storyView={this.storyView} />: null}
            {this.state.isModalVisible === true ? <ModalApp changeModalView={this.changeModalView} isVisible={this.state.isModalVisible}/> : null}  
            <ImageBackground source={primaryTheme.$loginBackground} resizeMode={'stretch'} style={styles.mainBackground}> 
              <Animated.View style={[styles.main,{left:this.state.mainAnime}]}> 
                <TouchableOpacity onPress={()=>this.menuAnime()} style={{position:'absolute',top:0,right:10,zIndex:999999}}>
                  <Icons.Entypo name={'menu'} style={{ color: '#fff',fontSize:40}}/>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>alert('הוסף סטורי')} style={{position:'absolute',top:0,left:10,zIndex:999999}}>
                  <Icons.MaterialCommunityIcons name={'shape-circle-plus'} style={{ color: '#fff',fontSize:35}}/> 
                </TouchableOpacity>
                <MenuApp navigate={navigate}/> 
                <View style={styles.topView}>
                  <Text style={{height:'25%',color:'#fff',textAlign:'center',paddingTop:10,width:'100%'}}>{this.state.topTitle}</Text>
                  <View style={{width:'100%',height:'75%',backgroundColor:'rgba(255,255,255,0.2)'}}>
                    <StoryApp storyView={this.storyView}/> 
                  </View>
                </View>
                <View style={styles.mainScrollView}>
                 <MainView navigate={navigate}/>
                </View>
                {/* <ScrollView style={styles.mainScrollView}> 
                  <View style={styles.mainItem}>
                    <Text>asfazsfawfgasf</Text>
                  </View>
                  <View style={styles.mainItem}>
                    <Text>asfazsfawfgasf</Text>
                  </View>
                  <View style={styles.mainItem}>
                    <Text>asfazsfawfgasf</Text>
                  </View>
                  <View style={styles.mainItem}>
                    <Text>asfazsfawfgasf</Text>
                  </View> 
                </ScrollView>  */}
              </Animated.View>
            </ImageBackground>
          </Animated.View>
      );
    }
   
    handleKeyboardDidShow = (event) => {
      const { height: windowHeight } = Dimensions.get('window');
      const keyboardHeight = event.endCoordinates.height;
      const currentlyFocusedField = TextInputState.currentlyFocusedField();
      UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
        const fieldHeight = height;
        const fieldTop = pageY;
        const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight);
        if (gap >= 0) {
          return;
        }
        Animated.timing(
          this.state.shift,
          {
            toValue: gap - 100,
            duration: 500,
            useNativeDriver: true,
          }
        ).start();
      });
    }
    
    handleKeyboardDidHide = () => {
      Animated.timing(
        this.state.shift,
        {
          toValue: 0,
          duration: 500,
          useNativeDriver: true,
        }
      ).start();
    } 
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor:'#000',  
    },  
    main:{
      width:'100%',
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar - 50,
      position:'absolute',
      alignItems: 'center',
      justifyContent:'flex-start', 
    },
    mainBackground:{
      width:'100%',
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar - 50,
      top: primaryTheme.$deviceStatusBar,  
      backgroundColor:'#000',
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
    },   
    mainScrollView:{ 
      width:'100%',
      height:'100%',
      position:'absolute',
      top: 150,
      height:primaryTheme.$deviceHeight - 200 - primaryTheme.$deviceStatusBar,
    },
    labelView:{ 
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
    },   
    topView:{
      width: '100%',
      height: 143, 
      position:'absolute',
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'flex-start',
    },
    mainItem:{
      width: '100%',
      height: 200, 
    }
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  };
  // Exports
  export default connect(mapStateToProps)(MainSell);